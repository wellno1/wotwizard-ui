import Vue from "vue"

export default (context, inject) => {
	let liste_favoris = localStorage.favourites
		? JSON.parse(localStorage.favourites)
		: []

	let toggleFavourite = (uid, e) => {
		e.stopPropagation()
		if (!context.$favourites.list.includes(uid)) {
			context.$favourites.list.push(uid)
		} else {
			context.$favourites.list = context.$favourites.list.filter(
				(item) => item !== uid
			)
		}

		localStorage.favourites = JSON.stringify(context.$favourites.list)
	}

	let addFavorisArray = (certs, e) => {
		for (const cert in certs) {
			toggleFavourite(certs[cert].uid, e)
		}
	}

	inject(
		"favourites",
		Vue.observable({
			list: liste_favoris,
			toggleFavourite: toggleFavourite,
			addFavorisArray: addFavorisArray
		})
	)
}
