import { HttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'
import { from } from 'apollo-link'

import { ENDPOINT1 } from './endpoints'
import { cache } from '../cache'

export default ctx => {
  const ssrMiddleware = setContext((_, { headers }) => {
    if (process.client) return headers
    return {
      headers
    }
  })

  const httpLink = new HttpLink({
    uri: ENDPOINT1
  })

  const link = from([ssrMiddleware, httpLink])

  return {
    link,
    cache,
    // https://github.com/nuxt-community/apollo-module/issues/306#issuecomment-607225431
    defaultHttpLink: false
  }
}